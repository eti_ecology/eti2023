function parseSpreadSheet(data){
    var nodes = []
    var links = []
    var i = 0

    var clean = string => string.trim().toLowerCase()
    function uuid(item){
	return  item.kind!='question'? item.tag:clean(item.text)
    }
    nodes.push({
	uuid:'root',
	kind:'root',
	tag:'ETI',
	keywords:['ETI'],
	text:"ETI 2023 Workshop",
	comments:[]})
    
    data.forEach(d=>{
	i += 1 
	var item = {
	    kind: clean(d[0]),
	    tag: clean(d[1]),
	    keywords:d[2].split(",").map(clean),
	    text: d[3],
	    extra: d[4],
	    comments: d.slice(5).filter(d=>d.length)
	}
	item.uuid = uuid(item)
	item.category = item.kind
	nodes.push(item)
	if (item.kind == "question"){
	    //links.push({source:item.uuid, target:clean(item.tag)})
	    item.keywords.slice(0,1).forEach(k=>{
		links.push({source:item.uuid, target:k, kind:'questiontag'})
	    })
	}
	else{
	    //nodes.push(item)
	}
	if (item.kind == "category"){
	    links.push({source:item.uuid, target:'root', kind:'rootlink'})
	}
	
	if (item.kind == "tag"){
	    links.push({source:item.uuid, target:item.keywords[0], kind:'cattag'})
	}
    })

    //nodes = nodes.filter(d=>d.kind!='question')
    var uuid_set = new Set(nodes.map(d=>d.uuid))
    let nlinks = links.length
    links = links.filter(d=>uuid_set.has(d.source)&&uuid_set.has(d.target))
    
    let new_nlinks = links.length
    if (nlinks != new_nlinks){
	console.warn("Removed "+(nlinks-new_nlinks)+" invalid links")}
    nodes.reverse()
    return {nodes:nodes, links:links}
}


function ForceGraph({
    nodes, // an iterable of node objects (typically [{id}, …])
    links // an iterable of link objects (typically [{source, target}, …])
}, {
    nodeId = d => d.uuid, // given d in nodes, returns a unique identifier (string)
    nodeGroup = d => d.malthusian?0:1, // given d in nodes, returns an (ordinal) value for color
    nodeGroups, // an array of ordinal values representing the node groups
    nodeTitle = d=>d.title,  // given d in nodes, a title string
    nodeFill = "currentColor", // node stroke fill (if not using a group color encoding)
    nodeStroke = "#fff", // node stroke color
    nodeStrokeWidth = 1.5, // node stroke width, in pixels
    nodeStrokeOpacity = 1, // node stroke opacity
    nodeRadius = 6, // node radius, in pixels
    nodeStrength =  d =>  {
	if(d.data.kind=='root'){return -1}
	else if(d.data.kind=='tag'){return -80}
	else if(d.data.kind=='category'){return -80}
	else if(d.data.kind=='question'){return -80}
    },
    linkSource = d => d.source, // given d in links, returns a node identifier string
    linkTarget = d => d.target, // given d in links, returns a node identifier string
    linkStroke = "#999", // link stroke color
    linkStrokeOpacity = 0.6, // link stroke opacity
    linkStrokeWidth = 2, // given d in links, returns a stroke width in pixels
    linkStrokeLinecap = "round", // link stroke linecap
    linkStrength = d=> d.data.kind=='questiontag'?3:d.data.kind=="rootlink"?0.1:0.2,
    colors = d3.schemeTableau10, // an array of color strings, for the node groups
    width = 1200, // outer width, in pixels
    height = 800, // outer height, in pixels
    invalidation // when this promise resolves, stop the simulation
} = {}) {

    const bool_tags = []
    const bool_tag_color = d3.scaleOrdinal([true,false], ["#007bff", "#6c757d"])
    const keyword_color = d3.scaleOrdinal(['ETI'], d3.schemeSet3)
    const family_color = d3.scaleOrdinal([],  d3.schemeCategory10)
    const N = d3.map(nodes, nodeId)
    const LS = d3.map(links, linkSource)
    const LT = d3.map(links, linkTarget)
    if (nodeTitle === undefined) nodeTitle = (_, i) => N[i]
    const T = nodeTitle == null ? null : d3.map(nodes, nodeTitle)
    const C = d3.map(nodes, d=>d.html)
    const LC = d3.map(links, d=>d.html)
    const LH = d3.map(links, d=>d.title)
    const G = nodeGroup == null ? null : d3.map(nodes, nodeGroup).map(intern)
    const W = typeof linkStrokeWidth !== "function" ? null : d3.map(links, linkStrokeWidth)
    const L = typeof linkStroke !== "function" ? null : d3.map(links, linkStroke)

    // Replace the input nodes and links with mutable objects for the simulation.
    //nodes = nodes.filter(d=>d.kind!='question')
    function init_x(data,i){
	if(data.kind=='root'){return 0}
	else if(data.kind=='tag'){return 50*nodeRadius*Math.cos(i)}
	else if(data.kind=='category'){return 20*nodeRadius*Math.sin(i)}
	else if(data.kind=='question'){return 80*nodeRadius*Math.cos(i)}
    }
    function init_y(data,i){
	if(data.kind=='root'){return 0}
	else if(data.kind=='tag'){return 50*nodeRadius*Math.sin(i)}
	else if(data.kind=='category'){return 20*nodeRadius*Math.sin(i)}
	else if(data.kind=='question'){return 800*nodeRadius*Math.sin(i)}
    }

    const sc = 120
    const fx = {'root':0, 'view':1*sc, 'kind':1*sc, 'abstract':-1*sc, 'mechanism':-1*sc}
    const fy = {'root':0, 'view':1*sc, 'kind':-1*sc, 'abstract':1*sc, 'mechanism':-1*sc}
    
    nodes = d3.map(nodes, (data, i) => ({id: N[i],
					 data:data,
					 fx:fx[data.uuid],
					 fy:fy[data.uuid],
					 x:init_x(data,i),
					 y:init_y(data,i)
					}))

    links = d3.map(links, (data, i) => ({source: LS[i], target: LT[i], data:data}))
    
    // Construct the forces.
    const forceNode = d3.forceManyBody()
    const forceLink = d3.forceLink(links).id(({index: i}) => N[i])
    if (nodeStrength !== undefined) forceNode.strength(nodeStrength)
    if (linkStrength !== undefined) forceLink.strength(linkStrength)

    // Create the structure
    const div = d3.create("div").attr("class","viz")
    const svg_container = div.append('div').attr("id","svg_container")
    const legend = svg_container.append('div').attr('id','legend').style('max-width',width+'px')
    const display = div.append('div').attr('id','display')
    const title = display.append('h1')
    const metadata = display.append('div')
    const content = display.append('div')
    
    function nodeSize(d){
	if(nodes[d.index].data.kind == "category"){return 6*nodeRadius}
	else if (nodes[d.index].data.kind == "tag"){return 4*nodeRadius}
	else if (nodes[d.index].data.kind == "root"){return 4*nodeRadius}
	else{return 2*nodeRadius}
    }

    function nodeSizeColl(d){
	return nodeSize(d)
	if(nodes[d.index].data.kind == "category"){return 3*nodeRadius}
	else if (nodes[d.index].data.kind == "tag"){return 2*nodeRadius}
	else{return nodeRadius}
    }


    const simulation = d3.forceSimulation(nodes)
	  .force("link", forceLink)
	  .force("charge", forceNode)
	  .force("collide", d3.forceCollide().radius(nodeSizeColl))
	  .force("center",  d3.forceCenter().strength(1))
	  .on("tick", ticked)
	  .tick(500)
    
   const svg = svg_container.append("svg")
	  .attr("width", width)
	  .attr("height", height)
	  .attr("viewBox", [-width / 2, -height / 2, width, height])
	  .attr("style", "max-width: 100%; height: auto; height: intrinsic;")
	  .on('click', _=> {window.location.hash=''; welcome()})
    const link = svg.append("g")
	  .attr("stroke", typeof linkStroke !== "function" ? linkStroke : null)
	  .attr("stroke-opacity", linkStrokeOpacity)
    
	  .attr("stroke-width", typeof linkStrokeWidth !== "function" ? linkStrokeWidth : null)
	  .attr("stroke-linecap", linkStrokeLinecap)
	  .selectAll("line")
	  .data(links)
	  .join("line")

     
    const node_grp = svg.append("g").attr("id","nodes")
    const node = node_grp.selectAll("g")
	  .data(nodes)
	  .enter()
	  .append("g")
	  .call(drag(simulation))

    node.append("circle")
    	.attr("r", nodeSize)
    	.attr("fill", nodeFill)
	.attr("stroke", nodeStroke)
	//.style('opacity',0.8)
	.attr("stroke-opacity", nodeStrokeOpacity)
	.attr("stroke-width", nodeStrokeWidth)
    
    node.append("text")
	.text(d=>nodes[d.index].data.kind=="question"?'':nodes[d.index].data.text)
	.style("text-anchor", "middle")
	.style("font-family", "Sans")
	.style("font-weight", "bold")
     
    if (W) link.attr("stroke-width", ({index: i}) => W[i])
    if (L) link.attr("stroke", ({index: i}) => L[i])
    //if (G) node.selectAll("circle").attr("fill", ({index: i}) => color(G[i]))
    if (T) node.append("title").text(({index: i}) => T[i])
    if (invalidation != null) invalidation.then(() => simulation.stop())

    
    function color_node(data){
	if (data.kind=='category'){return family_color(data.kind)}
	if (data.kind=='root'){return family_color(data.kind)}
	else if (data.kind!='tag'){return keyword_color(data.keywords[0])}
	else if (data.kind='tag'){return keyword_color(data.tag)}
    }
    
    function color_by(highlight){
	color_scheme = keyword_color
	if(highlight != undefined){
	    var color = d => {
		try{
		    return (d.tag==highlight || d.keywords.includes(highlight))? keyword_color(highlight):"#aaa"}
	    catch{error=>{console.log(d); console.error(error)}
		 }}
	}else{
	    var color = color_node 
	}
	tag = 'Legend:'
	node.selectAll('circle').attr("fill",
				      ({index: i}) => color(nodes[i].data))
	legend.html('Legend: ')
	legend.selectAll('span')
	    .data(keyword_color.domain())
	    .join('span')
	    .attr('class','tag')
	    .style('background-color', d=>color_scheme(d))
	    .text(d=>d)
	    .on('mouseenter', (e,d)=>color_by(d))
	    .on('mouseleave', (e,d)=>color_by())
    }

        
    function mouseover(event, datum){
	if (datum){window.location.hash = datum.data.uuid}
	title.html(nodes[datum.index].data.text)
	if(nodes[datum.index].data.extra!=undefined){
	try{
	    content.html(marked.parse(nodes[datum.index].data.extra))
	}catch(error){
	    console.error("Marked error: ", error)
	    content.html(nodes[datum.index].data.extra)
	}}
	metadata.html('')
	let tags = datum.data.keywords
	    .filter(d=>d.length)
	    .map(k=>nodes.find(d=>k==d.data.tag))
	let taglist = metadata.append('ul')
	 taglist.selectAll('li')
	    .data(tags)
	    .join('li')
	    .attr('class', 'tag')
	    .text(d=>d.data.text)
	    .style('background-color', d=>color_node(d.data))
	    .on('mouseenter', (e,d)=>color_by(d.data.tag))
	    .on('mouseleave', (e,d)=>color_by())

	let comments = content.append("div")
	let follow = content.append("div")
	let questions = datum.data.kind=='tag'?nodes.filter(d=>d.data.keywords.includes(datum.data.tag)):[]

	if (nodes[datum.index].data.comments.length){
	    comments.append('h3').text('Comments:')
	    comments.append('div')
		.selectAll('div')
		.data(nodes[datum.index].data.comments)
		.join('div')
		.attr('class','card')
		.text(d=>d)

	}
	
	if (questions.length){
	    follow.append('h4').text(`${questions.length} questions:`)
	    follow.append('ul')
		.style('max-height','15em')
	    	.style('overflow-y','scroll')
		.selectAll('li')
		.data(questions)
		.join('li')
		.style('background-color',d=>color_node(d.data))
		.text(d=>d.data.text)
		.on('click', mouseover)
	}
	content.append('div').attr('class','tip')
	    .text('Hover a node to display it, click a tag to change the color-scheme, click the graph to come back to the welcome page.')
	
    }
    function mouseover_link(event, datum){
    }
    
    node.on("mouseover",  mouseover)
    link.on("mouseover",  mouseover_link)
    
    function intern(value) {
	return value !== null && typeof value === "object" ? value.valueOf() : value
    }

    const lowx =  -width/2 + nodeRadius * 5
    const highx =  width/2 - nodeRadius * 5
    const lowy =  -height/2 + nodeRadius * 5
    const highy =  height/2 - nodeRadius * 5
    //const clip = (x,low,high) => (x<low?low:x>high?high:x)
    const clip = (x,low,high) => x
    function ticked() {
	link
	    .attr("x1", d => clip(d.source.x,lowx,highx))
	    .attr("y1", d => clip(d.source.y,lowy,highy))
	    .attr("x2", d => clip(d.target.x,lowx,highx))
	    .attr("y2", d => clip(d.target.y,lowy,highy))
	node.attr("transform", 
		  function (d) {
		      return "translate(" +
			  clip(d.x,lowx,highx)
			  + ", " +
			  clip(d.y,lowy,highy)
			  +")";});
    }

    function drag(simulation) {    
	function dragstarted(event) {
	    if (!event.active) simulation.alphaTarget(0.3).restart()
	    event.subject.fx = event.subject.x
	    event.subject.fy = event.subject.y
	}
    
	function dragged(event) {
	    event.subject.fx = event.x
	    event.subject.fy = event.y
	}
	
	function dragended(event) {
	    if (!event.active) simulation.alphaTarget(0)
	    event.subject.fx = null
	    event.subject.fy = null
	}	
	return d3.drag()
	    .on("start", dragstarted)
	    .on("drag", dragged)
	    .on("end", dragended)
    }

    function welcome(){
	title.html('Inquiry graph')
	color_by(undefined)
	metadata.html('')
	content.html('')
	content.append('h3').text('Categories')
	content.append('ul')
	    .selectAll('li')
	    .data(nodes.filter(d=>d.data.kind=='category'))
	    .join('li')
	    .text(d=>d.data.text)
	    .attr('class','tag')
	    .style('background-color', d=>color_node(d.data)) 
 	    .on('click', (e,d)=> {mouseover(e,d); color_by(d.data.tag)})
	    .on('mouseenter', (e,d)=>color_by(d.data.tag))
	    .on('mouseleave', (e,d)=>color_by())
	content.append('h3').text('Keywords')
	content.append('ul')
	    .selectAll('li')
	    .data(nodes.filter(d=>d.data.kind=='tag'))
	    .join('li')
	    .text(d=>d.data.text)
	    .attr('class','tag')
	    .style('background-color', d=>color_node(d.data)) 
	    .on('click', (e,d)=> {color_by(d.data.tag); mouseover(e,d);})
	    .on('mouseenter', (e,d)=>color_by(d.data.tag))
	    .on('mouseleave', (e,d)=>color_by())

	content.append('h3').text('Questions')
	content.append('ul')
	    .selectAll('li')
	    .data(nodes.filter(d=>d.data.kind=='question'))
	    .join('li')
	    .text(d=>d.data.text)
	    .attr('class','tag')
	    .style('background-color', d=>color_node(d.data)) 
	    .on('click', mouseover)
    }
    
    if (window.location.hash){
	let initial_node = nodes.find(node => node.data.uuid == window.location.hash.slice(1))
	if (initial_node != undefined){
	    color_by()
	    mouseover(undefined, initial_node)
	}
    }else{
	color_by('category', family_color)
        welcome()
    }
    color_by()
    return div
}


var graph
var ethercalcurl = "https://ethercalc.net/_/e1kxy3cow59o/csv.json"

function main(raw, source){
    d3.select('footer').append('span').html("| Data from "+source)
    console.log("RAW",  raw)
    data = parseSpreadSheet(raw)
    console.log("PARSED", data)
    graph = ForceGraph(data)
    d3.select('main').node().appendChild(graph.node())
}

function history(raw){
    console.log('history', raw)
    if (raw[window.location.search.slice(1)] != undefined) {
	let filename = 'archive/'+raw[window.location.search.slice(1)].filename
	let timestamp = raw[window.location.search.slice(1)].time
	d3.json(filename).then(d=>main(d, timestamp+" snapshot"))
    }
    else{
	var maindiv = d3.select('main').append('div')
	maindiv.append('h1').text('History')
	maindiv.append('p').text('Here is a list of archived versions of the inquiry graph.')
	var list = maindiv.append('ul')
	list.selectAll('li')
	    .data(Object.keys(raw))
	    .join('li')
	    .append('a')
	    .attr('href',d=>"?"+d)
	    .text(d=>raw[d].time)
    }
}

marked.use({
    mangle: false,
    headerIds: false  
})

console.log("Trying to fetch ", ethercalcurl)
if (window.location.pathname.split('/').pop() == 'history.html'){
    d3.json('history.json').then(history)
}else if (window.location.search == '?local'){
    d3.json("csv.json").then(d=>main(d, "local"))
}
    else{
fetch(ethercalcurl)
    .then(response => {
	console.log(response);
	try{
	    var value = response.json().then(d=> main(d, "live spreadsheet <a href='edit.html'>(Edit)</a>"))
	}
	catch(error){
	    console.log("ERROR IN TRY", error)
	    d3.json("csv.json").then(d=>main(d, "local cache"))
	}
    })
    .catch(error => {
	console.error("ERROR IN FETCH:", error)
	d3.json("csv.json").then(d=>main(d, "local cache"))
    })
}

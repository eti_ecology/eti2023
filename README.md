# Knowledge graph for the ETI 2023 Workshop



[https://workshops.evolbio.mpg.de/event/80/](worksop website)


## Content 
- `get_data.py` fetch the data from ethercalc and build a rudimentary archive database in `history.json`.
- `www/script.js` read data from ethercalc or fallback to a local datafile, and display the graph.
- `www/edit.js` is an iframe toward an ethercalc instance

## Ingredients:

- Python 
- Etherpad
- d3js 
- marked 


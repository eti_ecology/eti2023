"""Collect data
Run every half an hour with:

systemd-run --user --on-calendar "*-*-* *:00/30:00" --working-directory "research/2023_workshop/www/" python3 get_data.py
"""
import datetime
import os
import hashlib
import json
import urllib.request



historyfile = "www/history.json"
ethercalc = "https://ethercalc.net/_/e1kxy3cow59o/csv.json"
time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")

if os.path.exists(historyfile):
    with open(historyfile,'r') as fh:
        history = json.load(fh)
else:
    history = {}
    
datajson = urllib.request.urlopen(ethercalc).read()

h = hashlib.new('sha256')
h.update(datajson)
key = h.hexdigest()

if key in history:
    print(f"{key[:10]} already in the history")
else:
    filename = time.replace(' ','_')+".json"
    history[key] = dict(time=time, filename=filename)
    print(f"Save {key[:10]} as {filename}")
    with open(os.path.join('www/archive', filename),'wb') as fh:
        fh.write(datajson)

with open(historyfile,'w') as fh:
    json.dump(history, fh)
with open('www/data.json','wb') as fh:
    fh.write(datajson)

for k, v in history.items():
    print(k, v)
